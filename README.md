# proyecto-software-checkpoint-gt
Proyecto de Software para Checkpoint GT
Para correr este proyecto debe realizarse lo siguiente:
Tener instalado yarn
Primero instalar React Native
https://reactnative.dev/docs/environment-setup
npm install -g expo-cli

En la carpeta, del proyecto correr:
yarn install para instalar dependencias
yarn start para correr el proyecto

Luego, bajar aplicación de expo en cualquier celular
Escanear codigo QR que abrira la aplicación en EXPO para ser probada.
