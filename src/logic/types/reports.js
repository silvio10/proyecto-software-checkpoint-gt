export const FETCH_SALES_REPORT_BY_DATE_STARTED = 'FETCH_SALES_REPORT_BY_DATE_STARTED';
export const FETCH_SALES_REPORT_BY_DATE_COMPLETED = 'FETCH_SALES_REPORT_BY_DATE_COMPLETED';
export const FETCH_SALES_REPORT_BY_DATE_FAILED = 'FETCH_SALES_REPORT_BY_DATE_FAILED';